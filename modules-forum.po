#
# LANGUAGE translation of Drupal (modules-forum)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  forum-list.tpl.php,v 1.4 2007/08/30 18:58:12 goba
#  forum-submitted.tpl.php,v 1.3 2007/08/07 08:39:35 goba
#  forum-topic-navigation.tpl.php,v 1.2 2007/08/07 08:39:35 goba
#  forum.admin.inc,v 1.8 2008/01/30 10:14:42 goba
#  forum.module,v 1.448.2.2 2008/02/13 14:06:36 goba
#  forum.install,v 1.16 2007/12/31 16:58:34 goba
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2008-02-15 13:50+0100\n"
"PO-Revision-Date: 2008-06-12 17:50+0100\n"
"Last-Translator: Martin Benjamin <martin@kamusiproject.org>\n"
"Language-Team: Swahili <swahili@kamusiproject.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: modules/forum/forum-list.tpl.php:38
msgid "Topics"
msgstr "Mada"

#: modules/forum/forum-list.tpl.php:39
msgid "Posts"
msgstr "Ujumbe"

#: modules/forum/forum-list.tpl.php:40
msgid "Last post"
msgstr "Ujumbe wa mwisho"

#: modules/forum/forum-submitted.tpl.php:21
msgid "@time ago<br />by !author"
msgstr "@time kabla<br />na !author"

#: modules/forum/forum-topic-navigation.tpl.php:29
msgid "Go to previous forum topic"
msgstr "Nenda mada iliyotangulia katika baraza"

#: modules/forum/forum-topic-navigation.tpl.php:32
msgid "Go to next forum topic"
msgstr "Nenda mada ijayo katika baraza"

#: modules/forum/forum.admin.inc:38
msgid "Forum name"
msgstr "Jina la baraza"

#: modules/forum/forum.admin.inc:41
msgid "Short but meaningful name for this collection of threaded discussions."
msgstr "Jina fupi lenye maana kwa mkusanyiko huu wa mazungumzo katika minyororo."

#: modules/forum/forum.admin.inc:47
msgid "Description and guidelines for discussions within this forum."
msgstr "Maelezo na miongozi kwa mazungumzo katika baraza hili."

#: modules/forum/forum.admin.inc:54
msgid "Forums are displayed in ascending order by weight (forums with equal weights are displayed alphabetically)."
msgstr "Mabaraza yaoneshwa katika mpango kulingana na uzito (mabaraza yenye uzito sawa yaoneshwa kwa mpango kulingana na herufi ya kwanza katika alfabeti)."

#: modules/forum/forum.admin.inc:75
msgid "forum container"
msgstr "debe la baraza"

#: modules/forum/forum.admin.inc:79
#: modules/forum/forum.module:0
msgid "forum"
msgstr "baraza"

#: modules/forum/forum.admin.inc:90
msgid "Created new @type %term."
msgstr "@type %term mpya ilianzishwa."

#: modules/forum/forum.admin.inc:93
msgid "The @type %term has been updated."
msgstr "@type %term imesasishwa."

#: modules/forum/forum.admin.inc:116
msgid "Container name"
msgstr "Jina la debe"

#: modules/forum/forum.admin.inc:120
msgid "Short but meaningful name for this collection of related forums."
msgstr "Jina fupi lenye maana kwa kundi hili la mabaraza yanayohusiana."

#: modules/forum/forum.admin.inc:128
msgid "Description and guidelines for forums within this container."
msgstr "Maelezo na kanuni kwa mabaraza katika debe hili."

#: modules/forum/forum.admin.inc:136
msgid "Containers are displayed in ascending order by weight (containers with equal weights are displayed alphabetically)."
msgstr "Madebe yaoneshwa katika mpango kulingana na uzito (madebe yenye uzito sawa yaoneshwa kwa mpango kulingana na herufi ya kwanza katika alfabeti)."

#: modules/forum/forum.admin.inc:168
msgid "Are you sure you want to delete the forum %name?"
msgstr "Una hakika kwamba unataka kufuta baraza %name?"

#: modules/forum/forum.admin.inc:168
msgid "Deleting a forum or container will also delete its sub-forums, if any. To delete posts in this forum, visit <a href=\"@content\">content administration</a> first. This action cannot be undone."
msgstr "Ukifuta baraza au debe, pia utafuta baraza lolote chini lake.  Kufuta mafungu katika baraza hili, kwanza tembelea <a href=\"@content\">usimamizi wa viwekwo</a>. Hutaweza kutengua kitendo hiki."

#: modules/forum/forum.admin.inc:176
msgid "The forum %term and all sub-forums and associated posts have been deleted."
msgstr "Baraza %term limefutwa pamoja na mabaraza na mafungu yote chini lake."

#: modules/forum/forum.admin.inc:192
msgid "Hot topic threshold"
msgstr "Mpaka kwa mada joto"

#: modules/forum/forum.admin.inc:195
msgid "The number of posts a topic must have to be considered \"hot\"."
msgstr "Idadi ya maoni chini ya mada moja ili iitwe \"joto\"."

#: modules/forum/forum.admin.inc:199
msgid "Topics per page"
msgstr "Mada kwa ukurasa"

#: modules/forum/forum.admin.inc:202
msgid "Default number of forum topics displayed per page."
msgstr "Idadi ya awali ya mada za baraza zinazoonyeshwa katika kila ukurasa."

#: modules/forum/forum.admin.inc:204
msgid "Posts - most active first"
msgstr "Mafungu - maarufu zaidi kwanza"

#: modules/forum/forum.admin.inc:204
msgid "Posts - least active first"
msgstr "Mafungu - kimya zaidi kwanza"

#: modules/forum/forum.admin.inc:206
msgid "Default order"
msgstr "Mpango wa awali"

#: modules/forum/forum.admin.inc:209
msgid "Default display order for topics."
msgstr "Mpango wa awali kuonyesha mada."

#: modules/forum/forum.admin.inc:230
msgid "edit container"
msgstr "hariri debe"

#: modules/forum/forum.admin.inc:233
msgid "edit forum"
msgstr "hariri baraza"

#: modules/forum/forum.admin.inc:245
msgid "There are no existing containers or forums. Containers and forums may be added using the <a href=\"@container\">add container</a> and <a href=\"@forum\">add forum</a> pages."
msgstr "Hakuna madebe wala mabaraza yoyote.  Unaweza kuongeza madebe na mabaraza ukitumia kurasa <a href=\"@container\">ongeza debe</a> na <a href=\"@forum\">ongeza baraza</a>."

#: modules/forum/forum.admin.inc:286
msgid "Containers are usually placed at the top (root) level, but may also be placed inside another container or forum."
msgstr "Madebe huwekwa kwenye kiwango cha juu (mzizi), lakini pia unaweza kuyaweka katika debe au baraza lengine."

#: modules/forum/forum.admin.inc:289
msgid "Forums may be placed at the top (root) level, or inside another container or forum."
msgstr "Unaweza kuweka mabaraza kwenye kiwango cha juu (mzizi), au katika debe au baraza lengine."

#: modules/forum/forum.admin.inc:177
msgid "forum: deleted %term and all its sub-forums and associated posts."
msgstr "baraza: %term ilifutwa pamoja na mabaraza na mafungu yote chini yake."

#: modules/forum/forum.module:15
msgid "The forum module lets you create threaded discussion forums with functionality similar to other message board systems. Forums are useful because they allow community members to discuss topics with one another while ensuring those conversations are archived for later reference. The <a href=\"@create-topic\">forum topic</a> menu item (under <em>Create content</em> on the Navigation menu) creates the initial post of a new threaded discussion, or thread."
msgstr "Kijenzi cha mabaraza kinakuwezesha kufanya mabaraza yenye mazungumzo katika minyororo.  Mabaraza huwezesha watumiaji kujadiliana kuhusu mambo mbalimbali, na mazungumzo yao yatahifadhiwa kupatikana baadaye.  Chaguo <a href=\"@create-topic\">Mada ya baraza</a> (chini ya <em>Andika kiwekwo</em> kwenye menyu ya urambazaji) linaanzisha fungu la kwanza la zungumzo katika mnyororo, au uzi."

#: modules/forum/forum.module:16
msgid "A threaded discussion occurs as people leave comments on a forum topic (or on other comments within that topic). A forum topic is contained within a forum, which may hold many similar or related forum topics. Forums are (optionally) nested within a container, which may hold many similar or related forums. Both containers and forums may be nested within other containers and forums, and provide structure for your message board. By carefully planning this structure, you make it easier for users to find and comment on a specific forum topic."
msgstr "Zungumzo katika mnyororo linatokea wakati watumiaji waandika maoni kuhusu mada ya baraza (au kuhusu maoni mengine chini ya mada hiyo).  Mada ya baraza inakaa ndani ya baraza ambalo linaweza kujazwa na mada nyingi zinazohusiana au zinazolingana.  Mabaraza yanaweza kukaa ndani ya debe, linaloweza kujazwa na mabaraza mengi yanayolingana au yanayohusiana.  Madebe na mabaraza yanaweza kuwekwa ndani ya madebe na mabaraza mengine, ili kuleta muundo kwa mazungumzo katika tovuti yako.  Ukipanga muundo taratibu, utasaidia watumiaji kugundua na kujadiliana kuhusu mada za baraza mahsusi."

#: modules/forum/forum.module:17
msgid "When administering a forum, note that:"
msgstr "Ukisimamiza baraza, kumbuka kwamba:"

#: modules/forum/forum.module:18
msgid "a forum topic (and all of its comments) may be moved between forums by selecting a different forum while editing a forum topic."
msgstr "unaweza kuhamisha mada ya baraza (na maoni yake yote) baina ya mabaraza ukichagua baraza tofauti wakati ukihariri mada ya baraza."

#: modules/forum/forum.module:19
msgid "when moving a forum topic between forums, the <em>Leave shadow copy</em> option creates a link in the original forum pointing to the new location."
msgstr "ukihamisha mada ya baraza baina mabaraza, chaguo <em>Acha nakala muigo</em> linaweka kiungo katika baraza la awali kuelekea baraza jipya."

#: modules/forum/forum.module:20
msgid "selecting <em>Read only</em> under <em>Comment settings</em> while editing a forum topic will lock (prevent new comments) on the thread."
msgstr "Ukichagua <em>Soma tu</em> chini ya <em>Vipimo vya maoni</em> unapohariri mada katika baraza, utafunga mnyororo wa mazungumza (utazuia watumiaji kuandika maoni mapya chini ya fungu)."

#: modules/forum/forum.module:21
msgid "selecting <em>Disabled</em> under <em>Comment settings</em> while editing a forum topic will hide all existing comments on the thread, and prevent new ones."
msgstr "Ukichagua <em>Lemavu</em> chini ya <em>Vipimo vya maoni</em> unapohariri mada katika baraza, utaficha maoni yote kwenye mnyororo wa mazungumza na utazuia watumiaji kuandika maoni mapya chini ya fungu."

#: modules/forum/forum.module:22
msgid "For more information, see the online handbook entry for <a href=\"@forum\">Forum module</a>."
msgstr "Kwa maarifa mengine, tazama mwingilio kwa <a href=\"@forum\">Kijenzi cha baraza</a> katika kitabu cha maelezo mkondoni."

#: modules/forum/forum.module:25
msgid "This page displays a list of existing forums and containers. Containers (optionally) hold forums, and forums hold forum topics (a forum topic is the initial post to a threaded discussion). To provide structure, both containers and forums may be placed inside other containers and forums. To rearrange forums and containers, grab a drag-and-drop handle under the <em>Name</em> column and drag the forum or container to a new location in the list. (Grab a handle by clicking and holding the mouse while hovering over a handle icon.) Remember that your changes will not be saved until you click the <em>Save</em> button at the bottom of the page."
msgstr "Ukurasa huu unaonyesha orodha ya mabaraza na madebe. Madebe yanaweza kujazwa na mabaraza, na mabaraza hujazwa na mada za baraza (mada ya baraza ni fungu la kwanza kwenye zungumzo katika mnyororo).  Kwa muundo, unaweza kuweka madebe na mabaraza  ndani ya madebe na mabaraza mengine.  Kubadilisha mpango wa mabaraza na madebe, shika mpini wa kukota-na-kuangusha chini ya safu <em>Name</em>, na kokota baraza au debe kwenye sehemu mpya katika orodha.  (Kushika mpini, bonyeza na kamata puku wakati wa kuambaa juu ya picha ya mpini.)  Kumbuka kwamba mabadilisho yako hayatahifadhiwa mpaka ukibonyeza kitufe cha <em>Hifadhi</em> kinacho chini ya ukurasa."

#: modules/forum/forum.module:27
msgid "By grouping related or similar forums, containers help organize forums. For example, a container named \"Food\" may hold two forums named \"Fruit\" and \"Vegetables\", respectively."
msgstr "Madebe husaidia kupanga mabaraza katika makundi au mabaraza yanayofanana.  Kwa mfano, ndani ya debe linaloitwa \"Chakula\" unaweza kuweka mabaraza mawili yanayoitwa \"Matunda\" na \"Mboga\"."

#: modules/forum/forum.module:29
msgid "A forum holds related or similar forum topics (a forum topic is the initial post to a threaded discussion). For example, a forum named \"Fruit\" may contain forum topics titled \"Apples\" and \"Bananas\", respectively."
msgstr "Baraza hujazwa na mada za baraza zinazohusiana au zinazofanana (mada ya baraza ni fungu la kwanza kwenye zungumzo katika mnyororo).  Kwa mfano, ndani ya baraza linaloitwa \"Matunda\" watu waweza kuanza mada kuitwa \"Maembe\" na \"Ndizi\"."

#: modules/forum/forum.module:31
msgid "These settings allow you to adjust the display of your forum topics. The content types available for use within a forum may be selected by editing the <em>Content types</em> on the <a href=\"@forum-vocabulary\">forum vocabulary page</a>."
msgstr "Unaweza kutumia vipimo hivi kubadilisha maonyesho ya mada za baraza zako.  Unaweza kuchagua aina za viwekwo vitakavyopatikana katika baraza ukihariri <em>Aina za viwekwo</em> kwenye <a href=\"@forum-vocabulary\">ukurasa wa msamiati wa baraza</a>."

#: modules/forum/forum.module:230
msgid "The item %forum is only a container for forums. Please select one of the forums below it."
msgstr "%forum ni debe kwa mabaraza tu. Chagua baraza moja chini lake."

#: modules/forum/forum.module:296
msgid "Forum topic"
msgstr "Mada ya baraza"

#: modules/forum/forum.module:298
msgid "A <em>forum topic</em> is the initial post to a new discussion thread within a forum."
msgstr "<em>Mada katika baraza</em> ni fungu la kwanza la mnyororo wa mazungumzo mpya katika baraza."

#: modules/forum/forum.module:361
msgid "This is the designated forum vocabulary. Some of the normal vocabulary options have been removed."
msgstr "Hii ni msamiati rasmi ya baraza.  Baadhi ya machaguo ya msamiati ya kawaida yametolewa."

#: modules/forum/forum.module:403;419
msgid "Active forum topics"
msgstr "Mada maarufu za baraza"

#: modules/forum/forum.module:404;426
msgid "New forum topics"
msgstr "Mada mpya katika baraza"

#: modules/forum/forum.module:408
msgid "Number of topics"
msgstr "Idadi ya mada"

#: modules/forum/forum.module:435
msgid "Read the latest forum topics."
msgstr "Soma mada za hivi karibuni katika baraza."

#: modules/forum/forum.module:454
msgid "Leave shadow copy"
msgstr "Acha nakala muigo"

#: modules/forum/forum.module:454
msgid "If you move this topic, you can leave a link in the old forum to the new forum."
msgstr "Ukihamisha mada hii, unaweza kuacha kiungo kataka baraza lake la awali kuelekea baraza lake jipya."

#: modules/forum/forum.module:561
msgid "Topic"
msgstr "Mada"

#: modules/forum/forum.module:563
msgid "Created"
msgstr "Andikwa"

#: modules/forum/forum.module:564
msgid "Last reply"
msgstr "Jibu la mwisho"

#: modules/forum/forum.module:677
msgid "Post new @node_type"
msgstr "Andika @node_type mpya"

#: modules/forum/forum.module:684
msgid "You are not allowed to post new content in forum."
msgstr "Huruhusiwi kuandika mambo mapya katika baraza hili."

#: modules/forum/forum.module:688
msgid "<a href=\"@login\">Login</a> to post new content in forum."
msgstr "<a href=\"@login\">Ingia</a> kuandika mambo mapya katika baraza."

#: modules/forum/forum.module:726
msgid "No forums defined"
msgstr "Hakuna baraza lililoanzishwa"

#: modules/forum/forum.module:812
msgid "This topic has been moved"
msgstr "Mada haya yamehamishwa"

#: modules/forum/forum.module:322
msgid "create forum topics"
msgstr "anza mada katika baraza"

#: modules/forum/forum.module:322
msgid "delete own forum topics"
msgstr "futa mada zako katika baraza"

#: modules/forum/forum.module:322
msgid "delete any forum topic"
msgstr "futa mada yoyote katika baraza"

#: modules/forum/forum.module:322
msgid "edit own forum topics"
msgstr "hariri mada zako katika baraza"

#: modules/forum/forum.module:322
msgid "edit any forum topic"
msgstr "hariri mada yoyote katika baraza"

#: modules/forum/forum.module:322
msgid "administer forums"
msgstr "simamiza mabaraza"

#: modules/forum/forum.module:86;93
#: modules/forum/forum.install:27;116
msgid "Forums"
msgstr "Mabaraza"

#: modules/forum/forum.module:94
msgid "Control forums and their hierarchy and change forum settings."
msgstr "Simamia mabaraza na matabaka yao, na badilisha vipimo vya baraza."

#: modules/forum/forum.module:106
msgid "Add container"
msgstr "Ongeza debe"

#: modules/forum/forum.module:114
msgid "Add forum"
msgstr "Ongeza baraza"

#: modules/forum/forum.module:136
msgid "Edit container"
msgstr "Hariri debe"

#: modules/forum/forum.module:143
msgid "Edit forum"
msgstr "Hariri baraza"

#: modules/forum/forum.install:69
msgid "Stores the relationship of nodes to forum terms."
msgstr "Hifadhi mahusiano baina vitengo na istilahi ya baraza."

#: modules/forum/forum.install:76
msgid "The {node}.nid of the node."
msgstr "{node}.nid ya kitengo."

#: modules/forum/forum.install:90
msgid "The {term_data}.tid of the forum term assigned to the node."
msgstr "{term_data}.tid ya neno la baraza kwamba kitengo kilipewa."

